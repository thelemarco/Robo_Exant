import os

# Obter o caminho absoluto do script Python atual
caminho_script_atual = os.path.abspath(os.path.dirname(__file__))

# Concatenar com o subdiretório 'pdfs'
caminho_pdfs = os.path.join(caminho_script_atual, 'pdfs')

# Concatenar com o nome do arquivo
caminho_arquivo = os.path.join(caminho_pdfs, 'teste.pdf')

print(caminho_arquivo)
