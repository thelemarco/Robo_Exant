import pyautogui
import time
from pywinauto.application import Application
from pywinauto.findwindows import ElementNotFoundError

# Buscar imagem
imagem_alvo = 'ok.png'
timeout_imagem = 30  # tempo máximo de espera em segundos
intervalo_imagem = 1  # intervalo entre as verificações em segundos

inicio_imagem = time.time()
posicao = None

# Enquanto a imagem não for encontrada e o tempo de espera não tiver expirado
while posicao is None and time.time() - inicio_imagem < timeout_imagem:
    posicao = pyautogui.locateOnScreen(imagem_alvo)
    time.sleep(intervalo_imagem)  # aguarda o intervalo entre as verificações

# Verificar se a imagem foi encontrada
if posicao is not None:
    # Obter as coordenadas do centro da imagem
    centro = pyautogui.center(posicao)
    x, y = centro

    # Clicar na posição encontrada na janela de certificados digitais
    pyautogui.click(x, y)
    time.sleep(3)

    # Esperar pela janela
    titulo_da_janela = u'Introduzir PIN'
    timeout_janela = 30  # tempo máximo de espera em segundos
    intervalo_janela = 1  # intervalo entre as verificações em segundos

    inicio_janela = time.time()
    app = Application()
    dlg = None

    # Enquanto a janela não for encontrada e o tempo de espera não tiver expirado
    while dlg is None and time.time() - inicio_janela < timeout_janela:
        try:
            dlg = app.connect(title=titulo_da_janela)[titulo_da_janela]
        except ElementNotFoundError:
            time.sleep(intervalo_janela)  # aguarda o intervalo entre as verificações

    # Verificar se a janela foi encontrada
    if dlg is not None:
        time.sleep(0.5)
        with open('senha.txt', 'r') as file:
            senha_siape = file.read()

        dlg.type_keys(senha_siape)
        dlg.type_keys('{ENTER}')
    else:
        print("Janela não encontrada.")

else:
    print("Imagem não encontrada na tela.")
