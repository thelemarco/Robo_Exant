import urllib3
import requests
from pywinauto import *
import re
import tkinter as tk
from tkinter import messagebox

class Convert_String:

    def converter_string_para_strings(string_valor):
        # Remover caracteres indesejados: espaços e símbolos monetários
        caracteres_indesejados = [' ', 'R$', '€', '$', '¥', '£']
        for caractere in caracteres_indesejados:
            string_valor = string_valor.replace(caractere, '')

        # Substituir a vírgula por ponto se estiver presente
        string_valor = string_valor.replace(',', '.')

        # Separar os números antes e depois do ponto flutuante
        partes = string_valor.split('.')
        numero_esquerda = str(partes[0])
        numero_direita = partes[1] if len(partes) > 1 else '00'

        # Garantir que a string à direita tenha duas casas decimais
        numero_direita = numero_direita.ljust(2, '0')

        return numero_esquerda, numero_direita

class Convert_String_To_Float:

    def converter_string_para_float(string_valor):
        # Remover caracteres indesejados: espaços e símbolos monetários
        caracteres_indesejados = [' ', 'R$', '€', '$', '¥', '£']
        for caractere in caracteres_indesejados:
            string_valor = string_valor.replace(caractere, '')

        # Substituir a vírgula por ponto se estiver presente
        string_valor = string_valor.replace(',', '.')

        # Converter a string em float
        valor_float = float(string_valor)
        return valor_float

class Remove_Posicoes:

    def remover_posicoes(string, posicoes):
        # Converter a string em uma lista de caracteres
        caracteres = list(string)

        # Remover os caracteres nas posições especificadas
        for posicao in sorted(posicoes, reverse=True):
            del caracteres[posicao]

        # Juntar os caracteres restantes em uma string novamente
        nova_string = ''.join(caracteres)
        return nova_string

class Verificar_Servico:

    def verificar_servico(url):
        try:
            urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)
            response = requests.get(url, verify=False)
            if response.status_code == 200:
                print("O serviço está disponível e estável.")
            else:
                print(f"O serviço retornou o código de status: {response.status_code}")
        except requests.exceptions.RequestException as e:
            print("Não foi possível acessar o serviço:", e)

class Extrai_Numerais:

    def extrair_numerais(texto):
        numerais = ''
        for caractere in texto:
            if caractere.isdigit():
                numerais += caractere
        return numerais

class Exibi_Alerta:

    def exibir_alerta(valor1, valor2, valor3, valor4, valor5, valor6, valor7, valor8, valor9, valor10, valor11,
                      valor12, valor13, valor14):
        def fechar_janela():
            janela.destroy()

        janela = tk.Tk()
        janela.geometry("600x500")

        texto = f"Processo: {valor1}\nObjeto: {valor2}\nDescrição: {valor3}\nSituação Funcional: {valor4}\nÓrgão: " \
                f"{valor5}\nUpag: {valor6}\nMat Instituidor: {valor7}\nMeses: {valor8}\nAnos: {valor9}\nValor Esquerda:" \
                f" {valor10}\nValor Direita: {valor11}\nMat Beneficiário: {valor12}\nCota esquerda: {valor13}\nCota Direita: {valor14} "

        text_widget = tk.Text(janela, font=("Calibri", 12))
        text_widget.insert(tk.END, texto)
        text_widget.pack()

        # Configurar o espaçamento entre linhas
        text_widget.config(spacing2=10)
        text_widget.config(padx=20, pady=20)

        # Fechar a janela após 30 segundos
        janela.after(30000, fechar_janela)
        janela.mainloop()