import urllib3
import requests
from pywinauto import *
import re
import tkinter as tk
from tkinter import messagebox
import sys
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options as ChromeOptions
from selenium.webdriver.support.select import Select
import CisetClasses
import time
import classes_apoio
import siape
from minha import senha_Sei
from loginSiape import Acessa_siape
from siape import Troca_Hab
from siape import Inicia_Siape
from siape import Griaproadm
from siape import Griabenef

options = ChromeOptions()
options.add_experimental_option("detach", True)
navegador = webdriver.Chrome(options=options)
navegador.implicitly_wait(20)
navegador.maximize_window()

Loga_Siape = Acessa_siape()
troca_hab_instancia = Troca_Hab()
chama_siape = Inicia_Siape()
abre_proc_exant = Griaproadm()
lanca_exant = Griabenef()

meses = []
anos = []
valores = []
numero_esquerda = []
numero_direita = []
cota_esquerda = []
cota_direita = []
mat_bene = []
cot_parte = []
mat_inst = ""


def procura_marcador():
    # Seleciona a visualização por marcadores
    time.sleep(5)
    navegador.find_element(By.XPATH, '//*[@id="divFiltro"]/div[4]/a').click()
    # Seleciona marcador
    navegador.find_element(By.XPATH, '//*[@id="tblMarcadores"]/tbody/tr[25]/td[1]/a[2]').click()


# Acesso ao SEI
link = "https://sei.economia.gov.br/sip/modulos/MF/login_especial/login_especial.php?sigla_orgao_sistema=ME&sigla_sistema=SEI"

classes_apoio.Verificar_Servico.verificar_servico(link)

navegador.get(link)  # Acessa página inicial do SEI
campo_usu = navegador.find_element(By.ID, 'txtUsuario').send_keys(
    'marco.aurelio-silva@economia.gov.br')  # Entra Usuário
camp_senha = navegador.find_element(By.ID, 'pwdSenha').send_keys(senha_Sei)  # Entra Senha
navegador.find_element(By.XPATH, '//*[@id="selOrgao"]').send_keys('ME')  # Escolhe Ministério
navegador.find_element(By.XPATH, '//*[@id="Acessar"]').click()  # Acessa o SEI

time.sleep(2)
# Verifica janela de aviso e fecha
if navegador.find_element(By.XPATH, '/html/body/div[7]/div[2]/div[1]/div[3]/img'):  # Fecha Janela de aviso
    navegador.find_element(By.XPATH, '/html/body/div[7]/div[2]/div[1]/div[3]/img').click()

navegador.find_element(By.XPATH,
                       '/html/body/div[1]/nav/div/div[3]/div[2]/div[3]/div/a').click()  # Clica em selação da Unidade

navegador.find_element(By.XPATH, '//*[@id="selInfraOrgaoUnidade"]').send_keys('MGI')  # Escolhe Unidade

botao = navegador.find_element(By.XPATH, '//*[@id="chkInfraItem0"]').get_attribute(
    'checked')  # Verifica se a unidade está selecionada
if botao != 'true':
    navegador.find_element(By.XPATH, '/html/body/div[1]/div/div[2]/form/div[3]/table/tbody/tr[2]/td[2]').click()
else:
    navegador.find_element(By.XPATH, '/html/body/div[1]/nav/div/div[3]/div[2]/div[4]/a/img').click()

# Seleciona a visualização detalhada no controle de processos
navegador.find_element(By.XPATH, '/html/body/div[1]/div/div[2]/form/div/div[4]/div[1]/a').click()

procura_marcador()

# Busca por um processo específico
navegador.find_element(By.XPATH, '//*[@id="txtPesquisaRapida"]').click()
busca_proc = navegador.find_element(By.XPATH, '//*[@id="txtPesquisaRapida"]')
busca_proc.send_keys('14021.109953/2021-31')
busca_proc.send_keys(Keys.ENTER)

# Navega para o frame, árvore de documentos, verifica se a árvore precisa ser expandida, se sim, expande

navegador.switch_to.frame('ifrArvore')
process = navegador.find_element(By.CLASS_NAME, 'infraArvoreNoSelecionado').text.strip()

if navegador.find_elements(By.XPATH, '//img[@title="Abrir todas as Pastas"]'):
    navegador.implicitly_wait(10)
    navegador.find_element(By.XPATH, '//img[@title="Abrir todas as Pastas"]').click()

# Verífica a existencia de Planilha de Cálculo.
if navegador.find_elements(By.PARTIAL_LINK_TEXT, "Planilha de Cálculo"):
    navegador.implicitly_wait(3)
    plan = navegador.find_elements(By.PARTIAL_LINK_TEXT, "Planilha de Cálculo")
    plan[-1].click()
    navegador.switch_to.default_content()
    navegador.switch_to.frame('ifrVisualizacao')
    navegador.switch_to.frame('ifrArvoreHtml')
    objeto = classes_apoio.Extrai_Numerais.extrair_numerais(navegador.find_element(By.XPATH, '/html/body/table[3]/tbody/tr[2]/td[2]/p').text.strip())
    descricao = navegador.find_element(By.XPATH, '/html/body/table[3]/tbody/tr[4]/td[2]/p').text
    sit_func = navegador.find_element(By.XPATH, '/html/body/table[4]/tbody/tr[2]/td[2]/p').text.strip()
    orgao1 = navegador.find_element(By.XPATH, '/html/body/table[4]/tbody/tr[4]/td[2]/p').text.strip()
    upag = navegador.find_element(By.XPATH, '/html/body/table[4]/tbody/tr[5]/td[2]/p').text.strip()
    ultim_carac = upag[-1]
    upag1 = '0' * 8 + ultim_carac
    mat_inst = navegador.find_element(By.XPATH, '/html/body/table[4]/tbody/tr[6]/td[2]/p').text.strip()
    # mat_benef = navegador.find_element(By.XPATH, '/html/body/table[6]/tbody/tr[3]/td[2]/p').text.strip()
    # cota_parte = navegador.find_element(By.XPATH, '/html/body/table[6]/tbody/tr[4]/td[2]/p').text.strip()

    tabela = navegador.find_element(By.XPATH, '/html/body/table[5]')

    total = 0
    linhas = tabela.find_elements(By.TAG_NAME, 'tr')

    num_linhas_processadas = 0

    for linha in linhas:
        # Ignorar as duas primeiras linhas (cabeçalho)
        if num_linhas_processadas < 2:
            num_linhas_processadas += 1
            continue

        # Extrair as células da linha
        celulas = linha.find_elements(By.TAG_NAME, 'td')

        if celulas[0].text.strip() == "TOTAL":
            break  # Sai do loop se a célula [0] contém "TOTAL"

        if all(not celula.text.strip() for celula in celulas):
            continue  # Pula para a próxima iteração se a linha estiver vazia

        mes = celulas[0].text.strip()
        ano = celulas[1].text.strip()
        valor = celulas[4].text.strip()
        valEsq, valDir = classes_apoio.Convert_String.converter_string_para_strings(valor)
        total += classes_apoio.Convert_String_To_Float.converter_string_para_float(valor)

        # Adicionar os valores às listas correspondentes
        meses.append(mes)
        anos.append(ano)
        valores.append(valor)
        numero_esquerda.append(valEsq)
        numero_direita.append(valDir)

    tabela2 = navegador.find_element(By.XPATH, '/html/body/table[6]')

    total2 = 0
    linhas2 = tabela2.find_elements(By.TAG_NAME, 'tr')

    num_linhas_processadas2 = 0

    for linha2 in linhas2:
        # Ignorar as duas primeiras linhas (cabeçalho)
        if num_linhas_processadas2 < 2:
            num_linhas_processadas2 += 1
            continue

        # Extrair as células da linha
        celulas2 = linha2.find_elements(By.TAG_NAME, 'td')

        if all(not celula2.text.strip() for celula2 in celulas2):
            continue  # Pula para a próxima iteração se a linha estiver vazia

        matinee = celulas2[1].text.strip()
        cot_parte2 = celulas2[2].text.strip()
        esquerdaCot, direitaCot = classes_apoio.Convert_String.converter_string_para_strings(cot_parte2)

        # Adicionar os valores às listas correspondentes
        mat_bene.append(matinee)
        cota_esquerda.append(esquerdaCot)
        cota_direita.append(direitaCot)

    folha_comp = [folha + str(comp) for folha, comp in zip(meses, anos)]
    classes_apoio.Exibi_Alerta.exibir_alerta(process, objeto, descricao, sit_func, orgao1, upag1, mat_inst, meses, anos,
                                             numero_esquerda, numero_direita, mat_bene, cota_esquerda, cota_direita,)

    print(process)
    print(objeto)
    print(descricao)
    print(sit_func)
    print(orgao1)
    print(upag1)
    print(mat_inst)
    print(meses)
    print(anos)
    print(numero_esquerda)
    print(numero_direita)
    print(mat_bene)
    print(cota_esquerda)
    print(cota_direita)




    sys.exit()


else:
    print('Não foi possível encontrar planilha de cálculo neste processo')
    # sys.exit()

process_limpo = re.sub(r'[^a-zA-Z0-9]', '', process)
process_limpo = classes_apoio.Remove_Posicoes.remover_posicoes(process_limpo, [-1, -1, -3, -3])
process_parte1 = classes_apoio.Remove_Posicoes.remover_posicoes(process_limpo, [5, 6, 7, 8, 9, 10, 11, 12])
process_parte2 = classes_apoio.Remove_Posicoes.remover_posicoes(process_limpo, [0, 1, 2, 3, 4, 11, 12])
process_parte3 = classes_apoio.Remove_Posicoes.remover_posicoes(process_limpo, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
print(process_limpo)
print(process_parte1)
print(process_parte2)
print(process_parte3)

Loga_Siape.Login_Siape()

time.sleep(5)

time.sleep(5)
chama_siape.incio_siape()
troca_hab_instancia.troca_hab(orgao1, upag1)

dv_process = abre_proc_exant.griaproadm(process_parte1, process_parte2, process_parte3, objeto, descricao)
# dv_process = "63"

interval = 3

infoExtant = {'uniProc': process_parte1, 'idProc': process_parte2, 'anoProc': process_parte3,
              'devProc': dv_process, 'matInst': mat_inst, 'mesFol': meses, 'compFol': anos,
              'matBene': mat_bene, 'valorEsq': numero_esquerda, 'valorDir': numero_direita,
              'cotEsq': cota_esquerda, 'cotDir': cota_direita}
lanca_exant.griabenef(infoExtant)

sys.exit()
# Lança os percentuais por beneficiário
