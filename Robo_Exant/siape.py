from pywinauto import *
import CisetClasses
import time
import time
import keyboard
from pywinauto.application import Application
import classes_apoio

global intervalo
alerta = classes_apoio.Exibi_Alerta

class Inicia_Siape:

    def incio_siape(self):
        app = Application().start(
            r'javaws.exe -localfile "C:\Users\01467856738\AppData\LocalLow\Sun\Java\Deployment\cache\6.0\14\52bb3ace-3ce5febb"')
        time.sleep(25)
        tentativas = 0
        while not Application().connect(title='Painel de controle'):
            alerta.exibir_alerta("Aguardando conexão com o SIAPE")
            tentativas += 1
            if tentativas >= 4:
                alerta.exibir_alerta("Não foi possível estabelecer uma conexão com o SIAPE")
            time.sleep(10)
        app = Application().connect(title='Painel de controle')
        dlg = app['Painel de controle']
        dlg.type_keys('{TAB}')
        dlg.type_keys('{TAB}')
        dlg.type_keys('{TAB}')
        dlg.type_keys('{TAB}')
        dlg.type_keys('{TAB}')
        dlg.type_keys('{ENTER}')

        app = Application().connect(title='Terminal 3270 - A - AWVAD5YL')
        time.sleep(2)
        dlg = app['Terminal 3270 - A - AWVAD5YLTerminal 3270 - A - AWVAD5YL']
        dlg.type_keys('{F3}')
        dlg.type_keys('{F2}')


class Troca_Hab:
    def troca_hab(self, orgao1, upag1):
        app = Application().connect(title='Terminal 3270 - A - AWVAD5YL')
        dlg = app['Terminal 3270 - A - AWVAD5YLTerminal 3270 - A - AWVAD5YL']
        dlg.type_keys('TROCAHAB')
        dlg.type_keys('{ENTER}')
        orgao2 = orgao1
        upag2 = upag1
        orgao_upag = orgao2 + ' ' + upag2

        Acesso = CisetClasses.janela3270('Terminal 3270 - A - AWVAD5YL')
        Tela_Trocahab = Acesso.ViraTelaSiafiTecla('PF1')
        Tela_Trocahab = Acesso.ViraTelaSiafiTecla('ENTER')

        for l in range(12, 19):
            alvo = Acesso.PegaTextoSiafi(Tela_Trocahab, l, 11, l, 25)
            print(l)

            print(alvo)

            if alvo != orgao_upag:
                dlg.type_keys('{TAB}')
                continue
            break

        dlg.type_keys('X')
        dlg.type_keys('{ENTER}')
        dlg.type_keys('S')
        dlg.type_keys('{ENTER}')
        dlg.type_keys('{F2}')


class Griaproadm:

    def griaproadm(self, processo_parte1, processo_parte2, processo_parte3, objeto, descricao):
        app = Application().connect(title='Terminal 3270 - A - AWVAD5YL')
        time.sleep(intervalo)
        dlg = app['Terminal 3270 - A - AWVAD5YLTerminal 3270 - A - AWVAD5YL']
        dlg.type_keys('>GRIAPROADM')
        dlg.type_keys('{ENTER}')
        dlg.type_keys(processo_parte1)
        dlg.type_keys(processo_parte2)
        dlg.type_keys(processo_parte3)

        griapro = CisetClasses.janela3270('Terminal 3270 - A - AWVAD5YL')
        griapro.ViraTelaSiafiTecla('ENTER')
        time.sleep(intervalo)
        proc_dv_exant = griapro.PegaTextoSiafi(griapro.Tela, 7, 45, 7, 46).strip()
        dlg.type_keys(objeto)
        dlg.type_keys(descricao)
        dlg.type_keys('{ENTER}')
        dlg.type_keys('S')
        dlg.type_keys('{ENTER}')
        print(proc_dv_exant)
        dlg.type_keys('{F3}')
        dlg.type_keys('{F2}')
        return proc_dv_exant


class Griabenef:
    def griabenef(self, infoExtant):

        process_parte1 = infoExtant['uniProc']
        process_parte2 = infoExtant['idProc']
        process_parte3 = infoExtant['anoProc']
        dv_process = infoExtant['devProc']
        mat_inst = infoExtant['matInst']
        meses = infoExtant['mesFol']
        anos = infoExtant['compFol']
        valor_esq = ['valorEsq']
        valor_dir = ['valorDir']
        mat_bene = ['matBene']
        cotEsq = ['cotEsq']
        cotDir = ['cotDir']

        interval = 3
        app = Application().connect(title='Terminal 3270 - A - AWVAD5YL')
        time.sleep(interval)
        dlg = app['Terminal 3270 - A - AWVAD5YLTerminal 3270 - A - AWVAD5YL']
        # Entra na tela de lançamento em exercícios anteriores
        dlg.type_keys('>GRALBENEF')
        #dlg.type_keys('>GRIABENEF')
        dlg.type_keys('{ENTER}')
        time.sleep(interval)
        # Entra com o número do processo e o dv
        dlg.type_keys(process_parte1)
        dlg.type_keys(process_parte2)
        dlg.type_keys(process_parte3)
        dlg.type_keys(dv_process)
        dlg.type_keys('{ENTER}')
        time.sleep(interval)
        # Entra com a matrícula do instituidor / aposentado / reformado
        dlg.type_keys(mat_inst)
        dlg.type_keys('{ENTER}')
        time.sleep(interval)
        # Entra com os parametros de data inicial e data final
        dlg.type_keys(meses[0])
        dlg.type_keys(anos[0])
        dlg.type_keys(meses[-1])
        dlg.type_keys(anos[-1])
        dlg.type_keys('{ENTER}')
        time.sleep(interval)
        dlg.type_keys('S')
        dlg.type_keys('{ENTER}')
        time.sleep(interval)
        # Entra com os valores a serem lançados
        for indice, valor in enumerate(valor_esq):
            dlg.type(valor)
            dlg.type_keys('{TAB}')
            time.sleep(interval)
            dlg.type(valor_dir[indice])
            time.sleep((interval))
        dlg.type_keys('{ENTER}')
        time.sleep(interval)
        # Seleciona os beneficiários que constam na planilha
        griabene = CisetClasses.janela3270('Terminal 3270 - A - AWVAD5YL')
        griabene.ViraTelaSiafiTecla('ENTER')
        time.sleep(interval)

        espacos_em_branco = " " * 8  # espaços em branco consecutivos
        axisLina = 10
        while True:
            mat_benefits = griabene.PegaTextoSiafi(griabene.Tela, axisLina, 6, axisLina, 13)

            if mat_benefits == espacos_em_branco:
                time.sleep(interval)
                break  # Sai do loop se encontrar oito espaços em branco consecutivos

            if mat_benefits in mat_bene:
                dlg.type_keys('X')
                griabene.ViraTelaSiafiTecla('PF2')
                axisLina += 1
                time.sleep(0.5)
            else:
                dlg.type_keys('{TAB}')
                griabene.ViraTelaSiafiTecla('PF2')
                axisLina += 1
                time.sleep(interval)

        # Lança os percentuais por beneficiário

        axisLina2 = 10
        while True:
            mat_benefits = griabene.PegaTextoSiafi(griabene.Tela, axisLina, 4, axisLina, 11)
            time.sleep(interval)
            if mat_benefits == espacos_em_branco:
                time.sleep(interval)
                break  # Sai do loop se encontrar oito espaços em branco consecutivos
            time.sleep(interval)
            if mat_benefits in mat_bene:
                indices_2 = mat_bene.index(mat_benefits)

                dlg.type_keys(cotEsq[indices_2])
                if cotEsq[indices_2] != "100":
                    dlg.type_keys('{TAB}')
                time.sleep(interval)
                dlg.type_keys(cotDir[indices_2])
                griabene.ViraTelaSiafiTecla('PF2')
                axisLina2 += 1
                time.sleep(interval)
            else:
                dlg.type_keys('{TAB}')
                griabene.ViraTelaSiafiTecla('PF2')
                axisLina2 += 1
                time.sleep(interval)

        sys.exit()


class Imprime_Tela:
    def imprime_tela(self):
        # Localizar a janela pelo título
        window_title = "Terminal 3270 - A - AWVAD5YL"
        app = Application().connect(title=window_title)

        # Obter a janela
        window = app.window(title=window_title)

        # Ativar a janela
        window.set_focus()

        # Pressionar a tecla Alt para abrir o menu
        keyboard.press("alt")
        keyboard.press("a")
        time.sleep(0.1)
        keyboard.release("a")
        keyboard.release("alt")

        # Pressionar as teclas de seta para navegar até o menu "Arquivo"
        keyboard.press("Right")
        time.sleep(0.1)
        keyboard.release("Right")

        # Pressionar as teclas de seta para navegar até o subitem de menu "Imprimir Tela"
        keyboard.press("Down")
        time.sleep(0.1)
        keyboard.release("Down")

        # Pressionar as teclas de seta para navegar até o subitem de menu "Imprimir Tela"
        keyboard.press("Down")
        time.sleep(0.1)
        keyboard.release("Down")

        # Pressionar as teclas de seta para navegar até o subitem de menu "Imprimir Tela"
        keyboard.press("Down")
        time.sleep(0.1)
        keyboard.release("Down")

        # Pressionar as teclas de seta para navegar até o subitem de menu "Imprimir Tela"
        keyboard.press("Down")
        time.sleep(0.1)
        keyboard.release("Down")

        # Pressionar as teclas de seta para navegar até o subitem de menu "Imprimir Tela"
        keyboard.press("Down")
        time.sleep(0.1)
        keyboard.release("Down")

        # Pressionar a tecla Enter para selecionar o subitem de menu "Imprimir Tela"
        keyboard.press("Enter")
        time.sleep(0.1)
        keyboard.release("Enter")

        # Pressionar a tecla Enter para selecionar o subitem de menu "Imprimir Tela"
        keyboard.press("Enter")
        time.sleep(0.1)
        keyboard.release("Enter")
