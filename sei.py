import re
import sys
import time
from datetime import datetime
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.select import Select
from siape import SiapeGeral
from classes_apoio import Util_suport

class Sei_Geral:
    def tela_aviso(self, navegador):
        navegador.implicitly_wait(20)
        if navegador.find_element(By.XPATH, '/html/body/div[7]/div[2]/div[1]/div[3]/img'):  # Fecha Janela de aviso
            navegador.find_element(By.XPATH, '/html/body/div[7]/div[2]/div[1]/div[3]/img').click()

    def sel_unidade(self, navegador):
        # Clica em selação da Unidade
        navegador.implicitly_wait(20)
        navegador.find_element(By.XPATH, '/html/body/div[1]/nav/div/div[3]/div[2]/div[3]/div/a').click()
        time.sleep(0.5)
        navegador.find_element(By.XPATH, '//*[@id="selInfraOrgaoUnidade"]').send_keys('MGI')  # Escolhe Unidade
        time.sleep(0.5)
        botao = navegador.find_element(By.XPATH, '//*[@id="chkInfraItem0"]').get_attribute(
            'checked')  # Verifica se a unidade está selecionada
        if botao != 'true':
            navegador.find_element(By.XPATH, '/html/body/div[1]/div/div[2]/form/div[3]/table/tbody/tr[2]/td[2]').click()
            time.sleep(0.5)
        else:
            navegador.find_element(By.XPATH, '/html/body/div[1]/nav/div/div[3]/div[2]/div[4]/a/img').click()
            time.sleep(0.5)


    def visua_detal(self, navegador):
        # Seleciona a visualização detalhada no controle de processos
        navegador.implicitly_wait(20)
        navegador.find_element(By.XPATH, '/html/body/div[1]/div/div[2]/form/div/div[4]/div[1]/a').click()
        time.sleep(0.5)
    def procura_marcador(self, navegador):
        # Seleciona a visualização por marcadores
        navegador.implicitly_wait(20)
        time.sleep(0.5)
        navegador.find_element(By.XPATH, '//*[@id="divFiltro"]/div[4]/a').click()

        # Seleciona marcador
        time.sleep(0.5)
        navegador.find_element(By.XPATH, '//*[@onclick="filtrarMarcador(71471)"]').click()
        time.sleep(1)
        
        # navegador.find_element(By.CSS_SELECTOR, '#tblProcessosDetalhado > tbody > tr:nth-child(1) > th:nth-child(10) > div > div:nth-child(2) > a > img').click()
        # time.sleep(1)

    def procura_proc_esp(self, navegador):
        # Busca por um processo específico
        navegador.implicitly_wait(20)
        navegador.find_element(By.XPATH, '//*[@id="txtPesquisaRapida"]').click()
        busca_proc = navegador.find_element(By.XPATH, '//*[@id="txtPesquisaRapida"]')
        busca_proc.send_keys('14021.124589/2023-00')
        time.sleep(0.5)
        busca_proc.send_keys(Keys.ENTER)

    # def acessa_processo:(processo):
    #     #Acessa processo da lista
    #
    #
    # def busca_planilha(self):

    def num_processo(self, navegador):
        time.sleep(0.5)
        navegador.implicitly_wait(20)
        navegador.switch_to.frame('ifrArvore')
        process = navegador.find_element(By.CLASS_NAME, 'infraArvoreNoSelecionado').text.strip()
        return process

    def captura_dados(self, navegador, process):

        apoio = Util_suport()

        if navegador.find_elements(By.XPATH, '//img[@title="Abrir todas as Pastas"]'):
            navegador.implicitly_wait(2)
            navegador.find_element(By.XPATH, '//img[@title="Abrir todas as Pastas"]').click()
            time.sleep(0.5)

        if navegador.find_elements(By.PARTIAL_LINK_TEXT, "Comprovante de lançamento em módulo"):
            navegador.switch_to.default_content()
            navegador.switch_to.frame('ifrVisualizacao')
            navegador.find_element(By.XPATH, '//*[@id="divArvoreAcoes"]/a[25]/img').click()
            navegador.switch_to.default_content()
            message = "modulo"
            return message


        # Verífica a existencia de Planilha de Cálculo.
        if navegador.find_elements(By.PARTIAL_LINK_TEXT, "Planilha de Cálculo"):
            #navegador.implicitly_wait(3)

            meses = []
            anos = []
            valores = []
            numero_esquerda = []
            numero_direita = []
            cota_esquerda = []
            cota_direita = []
            nome_benes = []
            mat_bene = []
            cot_parte = []
            mat_inst = ""
            mes_anterior = None
            ano_anterior = None

            plan = navegador.find_elements(By.PARTIAL_LINK_TEXT, "Planilha de Cálculo")
            plan[-1].click()
            navegador.switch_to.default_content()
            navegador.switch_to.frame('ifrVisualizacao')
            navegador.switch_to.frame('ifrArvoreHtml')
            assunto = navegador.find_element(By.CSS_SELECTOR,
                                             "body > table:nth-child(6) > tbody > tr:nth-child(1) > td:nth-child(2)").get_attribute(
                'textContent').strip()
            objeto = apoio.extrair_numerais(navegador.find_element(By.CSS_SELECTOR,
                                                                   "body > table:nth-child(6) > tbody > tr:nth-child(2) > td:nth-child(2)").get_attribute(
                'textContent')).strip()
            descricao = navegador.find_element(By.CSS_SELECTOR,
                                               "body > table:nth-child(6) > tbody > tr:nth-child(4) > td:nth-child(2)").get_attribute(
                'textContent').strip()
            nome_inst = navegador.find_element(By.CSS_SELECTOR,
                                               "body > table:nth-child(8) > tbody > tr:nth-child(3) > td:nth-child(2)").get_attribute(
                'textContent').strip()
            sit_func = navegador.find_element(By.CSS_SELECTOR,
                                              "body > table:nth-child(8) > tbody > tr:nth-child(2) > td:nth-child(2)").get_attribute(
                'textContent').strip()
            orgao1 = navegador.find_element(By.CSS_SELECTOR,
                                            "body > table:nth-child(8) > tbody > tr:nth-child(4) > td:nth-child(2)").get_attribute(
                'textContent').strip()
            upag = navegador.find_element(By.CSS_SELECTOR,
                                          "body > table:nth-child(8) > tbody > tr:nth-child(5) > td:nth-child(2)").get_attribute(
                'textContent').strip()
            ultim_carac = upag[-1]
            upag1 = '0' * 8 + ultim_carac
            mat_inst = navegador.find_element(By.CSS_SELECTOR,
                                              "body > table:nth-child(8) > tbody > tr:nth-child(6) > td:nth-child(2)").get_attribute(
                'textContent').strip()

            tabela = navegador.find_element(By.XPATH, '/html/body/table[5]')

            total = 0
            linhas = tabela.find_elements(By.TAG_NAME, 'tr')

            num_linhas_processadas = 0

            for linha in linhas:
                # Ignorar as duas primeiras linhas (cabeçalho)
                if num_linhas_processadas < 2:
                    num_linhas_processadas += 1
                    continue

                # Extrair as células da linha
                celulas = linha.find_elements(By.TAG_NAME, 'td')

                if celulas[0].text.strip() == "TOTAL":
                    break  # Sai do loop se a célula [0] contém "TOTAL"

                if all(not celula.text.strip() for celula in celulas):
                    continue  # Pula para a próxima iteração se a linha estiver vazia

                mes = celulas[0].text.strip()
                ano = celulas[1].text.strip()
                valor = celulas[4].text.strip()
                valor2 = apoio.converter_string_para_float(valor)

                valEsq, valDir = apoio.converter_string_para_strings(valor2)
                # total += classes_apoio.Convert_String_To_Float.converter_string_para_float(valor)

                # Verificar se o mês e ano são iguais aos do registro anterior
                if mes == mes_anterior and ano == ano_anterior:
                    message = "Erro: Mês está duplicado."
                    return message

                # Adicionar os valores às listas correspondentes
                meses.append(mes)
                anos.append(ano)
                valores.append(valor)
                numero_esquerda.append(valEsq)
                numero_direita.append(valDir)

            tabela2 = navegador.find_element(By.XPATH, '/html/body/table[6]')

            total2 = 0
            linhas2 = tabela2.find_elements(By.TAG_NAME, 'tr')

            num_linhas_processadas2 = 0

            for linha2 in linhas2:
                # Ignorar as duas primeiras linhas (cabeçalho)
                if num_linhas_processadas2 < 2:
                    num_linhas_processadas2 += 1
                    continue

                # Extrair as células da linha
                celulas2 = linha2.find_elements(By.TAG_NAME, 'td')

                if all(not celula2.text.strip() for celula2 in celulas2):
                    continue  # Pula para a próxima iteração se a linha estiver vazia

                nome_bene = celulas2[0].text.strip()
                matinee = celulas2[1].text.strip()
                cot_parte2 = celulas2[2].text.strip()
                esquerdaCot, direitaCot = apoio.converter_string_para_strings(cot_parte2)
                info_list = [matinee, cot_parte2]

                # Adicionar os valores às listas correspondentes
                nome_benes.append(nome_bene)
                mat_bene.append(matinee)
                cota_esquerda.append(esquerdaCot)
                cota_direita.append(direitaCot)

                time.sleep(2)
            folha_comp = [folha + str(comp) for folha, comp in zip(meses, anos)]

            process_limpo = re.sub(r'[^a-zA-Z0-9]', '', process)
            process_limpo = apoio.remover_posicoes(process_limpo, [-1, -1, -3, -3])
            process_parte1 = apoio.remover_posicoes(process_limpo, [5, 6, 7, 8, 9, 10, 11, 12])
            process_parte2 = apoio.remover_posicoes(process_limpo, [0, 1, 2, 3, 4, 11, 12])
            process_parte3 = apoio.remover_posicoes(process_limpo, [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10])

            infoExtant = {'uniProc': process_parte1, 'idProc': process_parte2, 'anoProc': process_parte3,
                          'assunto': assunto, 'objeto': objeto, 'descricao': descricao,
                          'sit_func': sit_func, 'orgao': orgao1, 'upag': upag1, 'nome_inst': nome_inst,
                          'matInst': mat_inst,
                          'mesFol': meses, 'compFol': anos, 'nome_benes': nome_benes, 'matBene': mat_bene,
                          'valorEsq': numero_esquerda,
                          'valorDir': numero_direita, 'cotEsq': cota_esquerda, 'cotDir': cota_direita}

            # navegador.switch_to.default_content()
            # navegador.switch_to.frame('ifrArvore')
            # navegador.find_element(By.CLASS_NAME, 'noVisitado').click()
            # navegador.switch_to.default_content()
            # navegador.switch_to.frame('ifrVisualizacao')
            # navegador.find_element(By.XPATH, '//*[@id="divArvoreAcoes"]/a[25]/img').click()

            return infoExtant


        else:
            navegador.switch_to.default_content()
            navegador.switch_to.frame('ifrVisualizacao')
            navegador.find_element(By.XPATH, '//*[@id="divArvoreAcoes"]/a[25]/img').click()
            navegador.switch_to.default_content()
            message = False
            return message
            sys.exit()

    def inclui_comprov(self, navegador, arquiv):

        # concurrent_window = navegador.current_window_handle
        #
        # navegador.switch_to.window(concurrent_window)
        navegador.switch_to.default_content()
        navegador.switch_to.frame('ifrVisualizacao')
        navegador.find_element(By.XPATH, '//*[@id="divArvoreAcoes"]/a[1]/img').click()
        time.sleep(0.5)
        navegador.find_element(By.XPATH, '//*[@id="tblSeries"]/tbody/tr[1]/td/a[2]').click()
        time.sleep(0.5)
        navegador.find_element(By.XPATH, '//*[@id="selSerie"]').send_keys('Comprovante')
        time.sleep(0.5)
        navegador.find_element(By.XPATH, '//*[@id="txtDataElaboracao"]').send_keys(datetime.now().strftime('%d/%m/%Y'))
        time.sleep(0.5)
        navegador.find_element(By.XPATH, '//*[@id="txtNomeArvore"]').send_keys(
            ' de lançamento em módulo')
        time.sleep(0.5)
        navegador.find_element(By.XPATH, '//*[@id="divOptNato"]/div/label').click()
        time.sleep(0.5)
        navegador.find_element(By.XPATH, '//*[@id="divOptRestrito"]/div/label').click()
        time.sleep(0.5)
        drop = Select(navegador.find_element(By.XPATH, '//*[@id="selHipoteseLegal"]'))
        time.sleep(0.5)
        drop.select_by_visible_text("Informação Pessoal (Art. 31 da Lei nº 12.527/2011)")
        time.sleep(0.5)
        navegador.find_element(By.XPATH, '//*[@id="lblArquivo"]').click()

        arquivo = arquiv
        escolhe_arquivo = SiapeGeral()
        salva_arquivo = escolhe_arquivo.insere_comprovante(arquivo)
        navegador.find_element(By.XPATH, '//*[@id="btnSalvar"]').click()

        return True


